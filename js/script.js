(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.live_editor = {
    attach: function attach(context, settings) {

      var liveEditorInput = document.getElementById('live-editor-textarea');
      var editor = ace.edit('element');

      editor.setValue(liveEditorInput.value);
      editor.setOptions({
        fontSize: "12pt"
      });

      editor.setTheme("ace/theme/monokai");
      editor.session.setMode("ace/mode/" + drupalSettings.file_extension);
      editor.gotoLine(1,1, false);

      editor.on('change', function(e) {
        liveEditorInput.value = editor.getValue();
      });

      /**
       * Tree folder/files behavior
       **/
      function toggleElement(element){
        if(element.is(":visible")){
          element.hide();
        }
        else{
          element.show();
        }
      }

      $('#element').css('height', $(window).height() -250);
      $('#sidebar-menu').css('height', $(window).height() -262);
      $(window).on('resize', function(){
        $('#element').css('height', $(window).height() -250);
        $('#sidebar-menu').css('height', $(window).height() -262);
      });

      $('.folder-text').once().on('click', function(){
        if($(this).hasClass('element-open')){
          if($(this).next().children('.sidebar-subfolder')){
            toggleElement($(this).parent().find('.sidebar-subfolder').children('ul:visible'));
          }
          toggleElement($(this).next());
          $('.element-open').removeClass('element-open');
        }
        else{
          if($(this).next().children('.sidebar-subfolder')){
            $(this).parent().find('.sidebar-subfolder').children('ul:visible').hide();
          }
          $('.element-active').removeClass('element-active');
          $(this).addClass('element-active');
          toggleElement($('.element-active').next());
          $('.element-active').addClass('element-open');
        }
      });

      $('.sidebar-folder > span').each(function(){
        $(this).attr('value', drupalSettings.tree_path + '/' + $(this).children('.folder-text').text());
      });
      /**
       * END Tree folder/files behavior
       **/

      /**
       * contextual menu functionality
       **/
      var secondaryMenu = document.getElementById("secondary-menu");
      $('#secondary-menu').once().append('<input type="text" id="new-element">');
      var smenuSubActions = document.getElementById('new-element');

      function setSecondaryMenuPosition(element, x, y) {
        element.style.top = y + "px";
        element.style.left = x + "px";
        element.style.visibility = "visible";
        element.style.opacity = "1";
      }

      var modal = document.getElementById("confirmation-modal");
      var span = document.getElementsByClassName("close-modal")[0];

      $(document).once().on('click', function(e) {
        if($('#confirmation-modal').is(":hidden")){
          if ( (e.target.id != 'new-element')
            && (e.target.id != 'smenu-new-directory')
            && (e.target.id != 'smenu-new-file')
            && (e.target.className != 'close-modal')
            && (e.target.className != 'modal-cancel'))
          {
            smenuSubActions.style.display = 'none';
            secondaryMenu.style.opacity = "0";
            secondaryMenu.style.visibility = "hidden";
          }
        }
        if (e.target == modal) {
          modal.style.display = "none";
        }
      });

      /**  modal **/
      span.onclick = function() {
        modal.style.display = "none";
      }

      $('.modal-content').once().append('' +
        '<div class="modal-actions">' +
          '<input class="modal-accept" type="button" value="Accept">' +
          '<input class="modal-cancel" type="button" value="Cancel">' +
        '</div>');
      /** end modal **/

      var sidebarTree = document.getElementById('sidebar-menu');
      sidebarTree.oncontextmenu = function(e) {
        if(e.target.attributes.value != undefined){
          var UrlElement = e.target.attributes.value.textContent;
        }
        smenuSubActions.value = '';
        smenuSubActions.style.display = 'none';
        var elementClicked = e.target.textContent
        var targetElementClicked = e.target;

        var posX = e.clientX;
        var posY = e.clientY;
        setSecondaryMenuPosition(secondaryMenu, posX, posY);
        e.preventDefault();

        $('#smenu-new-directory').hide();
        $('#smenu-new-file').hide();
        $('#smenu-download').hide();
        $('#smenu-delete-directory').hide();
        $('#smenu-delete-file').hide();

        /** SECONDARY MENU OPTIONS **/
        if (e.target.parentElement.className == 'file-text' ) { //Files
          toggleElement($('#smenu-download'));
          toggleElement($('#smenu-delete-file'));
        }
        else if(e.target.id == 'sidebar-menu'){  //Root Path
          toggleElement($('#smenu-new-directory'));
        }
        else{
          toggleElement($('#smenu-new-directory')); //Directories
          toggleElement($('#smenu-new-file'));
          toggleElement($('#smenu-delete-directory'));
        }

        /** DOWNLOAD **/
        $('#smenu-download').attr('href', '/live-editor-download-file?url=' + UrlElement);

        $('#smenu-new-file').once().on('click', function(e){
          e.preventDefault();

          smenuSubActions.className = '';
          smenuSubActions.classList.add("new-file");
          smenuSubActions.style.display = 'flex';
          smenuSubActions.placeholder = 'File name...';
        })

        $('#smenu-new-directory').once().on('click', function(e){
          e.preventDefault();

          smenuSubActions.className = '';
          smenuSubActions.classList.add("new-directory");
          smenuSubActions.style.display = 'flex';
          smenuSubActions.placeholder = 'Directory name...';
        });

        $('#new-element').on('keypress',function(e) {
          var placeHolder = $(this).attr('placeholder');
          var newElementName = $(this).val();

          if (e.which == 13) {
            e.preventDefault();

            /** CREATE FILE **/
            if(placeHolder == 'File name...'){
              if (targetElementClicked.id == 'sidebar-menu'){
                $('#smenu-new-file').attr('href', '/live-editor-create-file?file_name=' + newElementName
                  + '&url=' + drupalSettings.tree_path);
              }
              else{
                $('#smenu-new-file').attr('href', '/live-editor-create-file?file_name=' + newElementName
                  + '&url=' + UrlElement + '/' + elementClicked);
              }

              modal.style.display = "block";
              $('.modal-text').html('Do you want to create the file: <strong>' + newElementName + '</strong>?');

              $('.modal-accept').on('click', function(){
                window.location=document.getElementById('smenu-new-file').href;
              });
              $('.modal-cancel').on('click', function(){
                modal.style.display = "none";
              });
            }
            else{
              /** CREATE DIRECTORY **/
              if (targetElementClicked.id == 'sidebar-menu') {
                $('#smenu-new-directory').attr('href', '/live-editor-create-directory?directory_name='
                  + newElementName + '&url=' + drupalSettings.tree_path);
              }
              else {
                $('#smenu-new-directory').attr('href', '/live-editor-create-directory?directory_name='
                  + newElementName + '&url=' + UrlElement + '/' + elementClicked);
              }

              modal.style.display = "block";
              $('.modal-text').html('Do you want to create the directory: <strong>' + newElementName + '</strong>?');

              $('.modal-accept').on('click', function(){
                window.location=document.getElementById('smenu-new-directory').href;
              });
              $('.modal-cancel').on('click', function(){
                modal.style.display = "none";
              });
            }
          }
        });

        /** DELETE FILE **/
        $('#smenu-delete-file').once().on('click', function(e){
          e.preventDefault();
          $('#smenu-delete-file').attr('href', '/live-editor-delete-file?url=' + UrlElement);

          modal.style.display = "block";
          $('.modal-text').html('Are you sure you want to delete the file: <strong>' + elementClicked + '</strong>?');

          $('.modal-accept').on('click', function(){
            window.location=document.getElementById('smenu-delete-file').href;
          });
          $('.modal-cancel').on('click', function(){
            modal.style.display = "none";
          });
        });

        /** DELETE FOLDER **/
        $('#smenu-delete-directory').once().on('click', function(e){
          e.preventDefault();
          $('#smenu-delete-directory').attr('href', '/live-editor-delete-directory?directory_name=' +
            elementClicked + '&url=' + UrlElement);

          modal.style.display = "block";
          $('.modal-text').html('Are you sure you want to delete the directory: <strong>' + elementClicked + '</strong>?');

          $('.modal-accept').on('click', function(){
            window.location=document.getElementById('smenu-delete-directory').href;
          });
          $('.modal-cancel').on('click', function(){
            modal.style.display = "none";
          });
        });
      };
      /**
       * contextual menu functionality
       **/
    }
  };
})(jQuery, Drupal, drupalSettings);
