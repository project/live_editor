<?php

namespace Drupal\live_editor\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Implements an example form.
 */
class LiveEditorForm extends FormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'live_editor_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $query_path = \Drupal::request()->get('path');
    $file_content = '';
    $config_live_editor = \Drupal::config('live_editor.settings');
    $tree_path = $config_live_editor->get('tree_path');

    if ($query_path != null) {
      $custom_doc = fopen($query_path, 'r+');

      $file_content = "";
      while ($line = fgets($custom_doc)) {
        $file_content .= $line;
      }
    }

    $form['file_name'] = [
      '#markup' => '<div class="actual-file-name"><span>' . str_replace($tree_path, '', $query_path) . '</span></div>',
    ];

    $form['edit_area'] = [
      '#type' => 'hidden',
      '#default_value' => $file_content,
      '#attributes' => [
        'id' => ['live-editor-textarea']
      ],
    ];

    $form['code_editor'] = [
      '#markup' => '<div id="element"></div>',
    ];

    $form['secondary_menu'] = [
      '#markup' => '
        <div id="secondary-menu">
            <a id="smenu-new-directory" href="#">'. t('New Directory') . '</a>
            <a id="smenu-new-file" href="#">'. t('New File') . '</a>
            <a id="smenu-download" href="#">'. t('Download') . '</a>
            <a id="smenu-delete-directory" href="#">'. t('Delete Directory') . '</a>
            <a id="smenu-delete-file" href="#">'. t('Delete File') . '</a>
        </div>
      ',
    ];

    $form['confirmation_modal'] = [
      '#markup' => '
        <div>
          <div id="confirmation-modal">
            <div class="modal-content">
              <span class="close-modal">&times;</span>
              <p class="modal-text"></p>
            </div>
          </div>
        </div>
    '];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    $form['#attached']['library'][] = 'live_editor/ace_library';
    $form['#attached']['drupalSettings']['file_extension'] = $this->getFileExtension($query_path);
    $form['#attached']['drupalSettings']['tree_path'] = $tree_path;

    $base_path = $_SERVER['DOCUMENT_ROOT'];
    $custom_modules_path = $base_path . '/modules/custom';
    $directories = array_diff(scandir($custom_modules_path), ['..', '.']);
    $tree = [];

    foreach ($directories as $directory) {
      $tree[$directory] = $this->getDirHtmlTree($custom_modules_path . '/' . $directory);
    }

    $form['file_tree'] = [
      '#theme' => 'menu_tree',
      '#menu_tree' => $tree,
    ];

    if ($query_path != null){
      fclose($custom_doc);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $new_value = $form_state->getValue('edit_area');
    $specific_file = $_GET['path'];
    $custom_doc = fopen($specific_file, 'w+');
    fputs($custom_doc, $new_value);
    fclose($custom_doc);
    if ($custom_doc == ''){
      \Drupal::messenger()->addError('No file selected, nothing to save.');
    } else {
      \Drupal::messenger()->addMessage('Changes saved successfully.');
    }

  }

  public function getDirHtmlTree($dir, &$tree = "")
  {
    $items = array_diff(scandir($dir), ['..', '.', '.git', 'live_editor']);

    foreach ($items as $item_name) {

      $path = realpath($dir . DIRECTORY_SEPARATOR . $item_name);

      $temp = explode('.',$item_name);
      $file_extension = end($temp);

      switch ($file_extension) {
        case "module":
        case "install":
        case "schema" :
        case "php" :
          $file_image = "php.svg";
          break;
        case "js":
          $file_image = "js.svg";
          break;
        case "css":
          $file_image = "css.svg";
          break;
        case "yml":
          $file_image = "yml.svg";
          break;
        case "png":
        case "svg":
        case "jpg":
        case "jpeg":
          $file_image = "image.svg";
          break;
        case "twig":
          $file_image = "twig.svg";
          break;
        case "json":
          $file_image = "json.svg";
          break;
        default:
          $file_image = "file.svg";
          break;
      }

      if (!is_dir($path)) {
        $tree .= "<li class='sidebar-file'><img src='/modules/custom/live_editor/images/" . $file_image
          . "' class='sidebar-file-img'/><a class='file-text' href='/admin/config/live_editor/edit?path=$path'><span value='" . dirname($path) . '/' . $item_name . "'>"
          . $item_name . "</span></a></li>";
      } else {
        $tree .= "<li class='sidebar-subfolder'><div class='folder-text subfolder'><img src='/modules/custom/live_editor/images/folder.svg'"
          . " class='sidebar-dir-img'/><span value='" . dirname($path) . "'>" . $item_name . "</span></div><ul>";
        $this->getDirHtmlTree($path, $tree);
        $tree .= "</ul></li>";
      }
    }

    return $tree;
  }

  public function getFileExtension($path)
  {
    $matches = array();
    $extension_pattern = '/\.[0-9a-z]+$/i';
    preg_match($extension_pattern, $path, $matches);
    $extension = str_replace('.', '', $matches[0]);

    switch ($extension) {
      case "module":
      case "install":
      case "schema" :
      case "inc":
        $extension = "php";
        break;
      case "js":
        $extension = "javascript";
        break;
      case "yml":
        $extension = "yaml";
        break;
      case "twig":
        $extension = "twig";
        break;
      case "json":
        $extension = "json";
        break;
      default:
    }

    return $extension;
  }

}
