<?php

namespace Drupal\live_editor\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements an example form.
 */
class LiveEditorConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'live_editor.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'live_editor_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config_live_editor = $this->config('live_editor.settings');

    $form['live_editor_tree_path'] = [
      '#type' => 'textfield',
      '#title' => t('Editor root path'),
      '#description' => t('Path to use as root for editor folder tree.'),
      '#default_value' => $config_live_editor->get('tree_path')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);



    $this->config('live_editor.settings')
      ->set('tree_path', $form_state->getValue('live_editor_tree_path'))
      ->save();
  }
}
