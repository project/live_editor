<?php

namespace Drupal\live_editor\Controller;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Bridge\PsrHttpMessage\Tests\Fixtures\Uri;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * An example controller.
 */
class LiveEditorController extends ControllerBase {

  /**
   * Download file
   **/
  public function downloadFile() {

    $uri = \Drupal::request()->get('url');
    $fileName = basename($uri);

    $mime = \Drupal::service('file.mime_type.guesser')->guess($uri);

    $headers = array(
      'Content-Type' => $mime . '; name="' . iconv_mime_decode($fileName) . '"',
      'Content-Length' => filesize($uri),
      'Content-Disposition' => 'attachment; filename="' . $fileName . '"',
      'Cache-Control' => 'private',
    );
    $uri_obj = new Uri($uri);
    \Drupal::messenger()->addMessage('File: ' . $fileName . ', successfully downloaded.');
    return new BinaryFileResponse($uri, 200, $headers, $uri_obj->getScheme() !== 'private');
  }

  /**
   * Create File/Folder
   **/
  public function createFile(){
    $fileName = \Drupal::request()->get('file_name');
    $uri = \Drupal::request()->get('url');
    $uri = str_replace('//','/',$uri);
    $filePath = $uri . '/' . $fileName;

    if(!file_exists($filePath)){
      $newFile = fopen($filePath, 'a+');
      \Drupal::messenger()->addMessage('File: ' . $fileName . ', successfully created.');
    } else {
      \Drupal::messenger()->addError('The file: ' . $fileName . ', already exists.');
    }

    fclose($filePath);

    $url = Url::fromRoute('live_editor.file_edit');
    return new RedirectResponse($url->toString());
  }

  public function createDirectory(){
    $directoryName = \Drupal::request()->get('directory_name');
    $uri = \Drupal::request()->get('url');
    $uri = str_replace('//','/',$uri);
    $dirPath = $uri . '/' . $directoryName;

    if (!file_exists($dirPath)) {
      mkdir($dirPath, 0777, true);
      \Drupal::messenger()->addMessage('Directory: ' . $directoryName . ', successfully created.');
    } else {
      \Drupal::messenger()->addError('The directory: ' . $directoryName . ', already exists.');
    }

    $url = Url::fromRoute('live_editor.file_edit');
    return new RedirectResponse($url->toString());
  }

  /**
   * Delete Folder
  **/

  public function deleteDirectoryRecursive($target){
    if (is_dir($target)) {
      $files = glob($target . '*', GLOB_MARK); //GLOB_MARK adds a slash to directories returned

      foreach ($files as $file) {
        $this->deleteDirectoryRecursive($file);
      }

      rmdir($target);
    }
    elseif (is_file($target)) {
      unlink($target);
    }

  }

  public function deleteDirectory(){
    $directoryName = \Drupal::request()->get('directory_name');
    $uri = \Drupal::request()->get('url');
    $uri = str_replace('//','/',$uri);
    $dirPath = $uri . '/' . $directoryName;

    $this->deleteDirectoryRecursive($dirPath);

    \Drupal::messenger()->addMessage('Directory: ' . $directoryName . ', successfully deleted.');
    $url = Url::fromRoute('live_editor.file_edit');
    return new RedirectResponse($url->toString());
  }

  /**
   * Delete Files
   **/
  public function deleteFile(){
    $uri = \Drupal::request()->get('url');
    $uri = str_replace('//','/',$uri);
    $fileName = basename($uri);

    unlink($uri);

    \Drupal::messenger()->addMessage('File: ' . $fileName . ', successfully deleted.');
    $url = Url::fromRoute('live_editor.file_edit');
    return new RedirectResponse($url->toString());
  }

}
